<?php

/**
 * @category Bitbull
 * @package  Bitbull_AvsFastsimpleimportPatch
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */
class Bitbull_AvsFastsimpleimportPatch_Model_ImportExport_Import_Entity_Product
    extends Mage_ImportExport_Model_Import_Entity_Product
{


    /**
     * Added dirty hack: extra method because module Avs_FastSimpleImport breaks native importer with the following error
     *
     * <b>Fatal error</b>: Call to undefined method Mage_ImportExport_Model_Import_Entity_Product::filterRowData() in <b>/var/www/magento/app/code/community/AvS/FastSimpleImport/Model/Import/Entity/Product/Type/Configurable.php</b> on line <b>166</b><br />
     *
     * @return $this
     */
    public function filterRowData()
    {
        return $this;
    }

}