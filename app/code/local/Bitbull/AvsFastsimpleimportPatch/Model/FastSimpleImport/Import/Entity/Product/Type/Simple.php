<?php
/**
 * @category Bitbull
 * @package  Bitbull_AvsFastsimpleimportPatch
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */

class Bitbull_AvsFastsimpleimportPatch_Model_FastSimpleImport_Import_Entity_Product_Type_Simple
    extends AvS_FastSimpleImport_Model_Import_Entity_Product_Type_Simple
{

    /**
     * Prepare attributes values for save: remove non-existent, remove empty values, remove static.
     *
     * @param array $rowData
     * @param bool $withDefaultValue
     * @return array
     */
    public function prepareAttributesForSave(array $rowData, $withDefaultValue = true)
    {
        $resultAttrs = array();

        foreach ($this->_getProductAttributes($rowData) as $attrCode => $attrParams) {
            if (!$attrParams['is_static']) {
                if (isset($rowData[$attrCode]) && strlen($rowData[$attrCode])) {
                    //added extra value checks
                    if ('select' == $attrParams['type'] || 'multiselect' == $attrParams['type']) {
                       if (isset($attrParams['options'][strtolower($rowData[$attrCode])])) {
                           $resultAttrs[$attrCode] = $attrParams['options'][strtolower($rowData[$attrCode])];
                       }
                    } else {
                        $resultAttrs[$attrCode] = $rowData[$attrCode];
                    }
                } elseif (array_key_exists($attrCode, $rowData)) {
                    $resultAttrs[$attrCode] = $rowData[$attrCode];
                } elseif ($this->_isSkuNew($rowData['sku'])) {
                    $defaultValue = $this->_getDefaultValue($attrParams);
                    if (null !== $defaultValue) {
                        $resultAttrs[$attrCode] = $defaultValue;
                    }
                }
            }
        }
        return $resultAttrs;
    }

}